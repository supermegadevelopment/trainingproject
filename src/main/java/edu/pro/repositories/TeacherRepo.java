package edu.pro.repositories;

import edu.pro.models.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TeacherRepo extends JpaRepository<Teacher, Long> {

    Optional<Teacher> findByUsername(String s);

    Optional<Teacher> findById(Long id);
}
