package edu.pro.repositories;

import edu.pro.models.File;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FileRepo extends JpaRepository<File, UUID> {
}
