package edu.pro.repositories;

import edu.pro.models.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRepo extends JpaRepository<Admin, Long> {

    Optional<Admin> findByUsername(String s);

    Optional<Admin> findById(Long id);
}
