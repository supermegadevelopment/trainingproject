package edu.pro.converters;

import edu.pro.dtos.StudentDto;
import edu.pro.models.Answer;
import edu.pro.models.Student;
import edu.pro.repositories.AnswerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class StudentConverter implements Converter<Student, StudentDto> {

    private final AnswerRepo answerRepo;

    @Autowired
    public StudentConverter(AnswerRepo answerRepo) {
        this.answerRepo = answerRepo;
    }

    @Override
    public StudentDto entityToDto(Student student) {
        return StudentDto.builder()
                .id(student.getId())
                .username(student.getUsername())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .username(student.getUsername())
                .password(student.getPassword())
                .role(student.getRole())
                .answerIds(student.getAnswers().stream().map(Answer::getId).collect(Collectors.toList()))
                .isNotBanned(student.getIsNotBanned())
                .build();
    }

    @Override
    public List<StudentDto> entityToDto(List<Student> students) {
        return students.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Student dtoToEntity(StudentDto studentDto) {
        return Student.builder()
                .id(studentDto.getId())
                .firstName(studentDto.getFirstName())
                .lastName(studentDto.getLastName())
                .username(studentDto.getUsername())
                .password(studentDto.getPassword())
                .role(studentDto.getRole())
                .answers(studentDto.getAnswerIds().stream().map(x -> answerRepo.findById(x).orElse(null)).filter(Objects::nonNull).collect(Collectors.toList()))
                .isNotBanned(studentDto.getIsNotBanned())
                .build();
    }

    @Override
    public List<Student> dtoToEntity(List<StudentDto> studentsDto) {
        return studentsDto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
