package edu.pro.converters;

import edu.pro.dtos.TeacherDto;
import edu.pro.models.Lecture;
import edu.pro.models.Teacher;
import edu.pro.repositories.LectureRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class TeacherConverter implements Converter<Teacher, TeacherDto> {

    private final LectureRepo lectureRepo;

    @Autowired
    public TeacherConverter(LectureRepo lectureRepo) {
        this.lectureRepo = lectureRepo;
    }

    @Override
    public TeacherDto entityToDto(Teacher teacher) {
        return TeacherDto.builder()
                .id(teacher.getId())
                .firstName(teacher.getFirstName())
                .lastName(teacher.getLastName())
                .username(teacher.getUsername())
                .password(teacher.getPassword())
                .role(teacher.getRole())
                .isNotBanned(teacher.getIsNotBanned())
                .lectureIds(teacher.getLectures().stream().map(Lecture::getId).collect(Collectors.toList()))
                .build();
    }

    @Override
    public List<TeacherDto> entityToDto(List<Teacher> teachers) {
        return teachers.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Teacher dtoToEntity(TeacherDto teacherDto) {
        return Teacher.builder()
                .id(teacherDto.getId())
                .firstName(teacherDto.getFirstName())
                .lastName(teacherDto.getLastName())
                .username(teacherDto.getUsername())
                .password(teacherDto.getPassword())
                .role(teacherDto.getRole())
                .isNotBanned(teacherDto.getIsNotBanned())
                .lectures(teacherDto.getLectureIds().stream().map(x -> lectureRepo.findById(x).orElse(null)).filter(Objects::nonNull).collect(Collectors.toList()))
                .build();
    }

    @Override
    public List<Teacher> dtoToEntity(List<TeacherDto> teachersDto) {
        return teachersDto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
