package edu.pro.converters;

import edu.pro.dtos.TestDto;
import edu.pro.models.Answer;
import edu.pro.models.Test;
import edu.pro.repositories.AnswerRepo;
import edu.pro.repositories.LectureRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class TestConverter implements Converter<Test, TestDto> {

    private final LectureRepo lectureRepo;
    private final AnswerRepo answerRepo;

    @Autowired
    public TestConverter(LectureRepo lectureRepo, AnswerRepo answerRepo) {
        this.lectureRepo = lectureRepo;
        this.answerRepo = answerRepo;
    }

    @Override
    public TestDto entityToDto(Test test) {
        return TestDto.builder()
                .id(test.getId())
                .lectureId(test.getLecture().getId())
                .question(test.getQuestion())
                .answerIds(test.getAnswers().stream().map(Answer::getId).collect(Collectors.toList()))
                .build();
    }

    @Override
    public List<TestDto> entityToDto(List<Test> tests) {
        return tests.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Test dtoToEntity(TestDto testDto) {
        return Test.builder()
                .id(testDto.getId())
                .lecture(lectureRepo.findById(testDto.getId()).orElse(null))
                .question(testDto.getQuestion())
                .answers(testDto.getAnswerIds().stream().map(x -> answerRepo.findById(x).orElse(null)).filter(Objects::nonNull).collect(Collectors.toList()))
                .build();
    }

    @Override
    public List<Test> dtoToEntity(List<TestDto> testsDto) {
        return testsDto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
