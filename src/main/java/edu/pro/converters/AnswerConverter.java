package edu.pro.converters;

import edu.pro.dtos.AnswerDto;
import edu.pro.models.Answer;
import edu.pro.repositories.StudentRepo;
import edu.pro.repositories.TestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AnswerConverter implements Converter<Answer, AnswerDto> {

    private final TestRepo testRepo;
    private final StudentRepo studentRepo;

    @Autowired
    public AnswerConverter(TestRepo testRepo, StudentRepo studentRepo) {
        this.testRepo = testRepo;
        this.studentRepo = studentRepo;
    }

    @Override
    public AnswerDto entityToDto(Answer answer) {
        return AnswerDto.builder()
                .id(answer.getId())
                .testId(answer.getTest().getId())
                .studentId(answer.getStudent().getId())
                .date(answer.getDate())
                .answerText(answer.getAnswerText())
                .build();
    }

    @Override
    public List<AnswerDto> entityToDto(List<Answer> answers) {
        return answers.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Answer dtoToEntity(AnswerDto answerDto) {
        return Answer.builder()
                .id(answerDto.getId())
                .test(testRepo.findById(answerDto.getTestId()).orElse(null))
                .student(studentRepo.findById(answerDto.getStudentId()).orElse(null))
                .date(answerDto.getDate())
                .answerText(answerDto.getAnswerText())
                .build();
    }

    @Override
    public List<Answer> dtoToEntity(List<AnswerDto> answersDto) {
        return answersDto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
