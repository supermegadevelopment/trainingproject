package edu.pro.converters;

import java.util.List;

public interface Converter<E, D> {
    public D entityToDto(E entity);

    public List<D> entityToDto(List<E> entities);

    public E dtoToEntity(D dto);

    public List<E> dtoToEntity(List<D> dtos);
}
