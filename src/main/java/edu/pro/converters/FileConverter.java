package edu.pro.converters;

import edu.pro.dtos.FileDto;
import edu.pro.models.File;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileConverter implements Converter<File, FileDto> {

    @Override
    public FileDto entityToDto(File file) {
        return FileDto.builder()
                .id(file.getId())
                .name(file.getName())
                .type(file.getType())
                .content(file.getContent())
                .build();
    }

    @Override
    public List<FileDto> entityToDto(List<File> files) {
        return files.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    @Override
    public File dtoToEntity(FileDto fileDto) {
        return File.builder()
                .id(fileDto.getId())
                .name(fileDto.getName())
                .type(fileDto.getType())
                .content(fileDto.getContent())
                .build();
    }

    @Override
    public List<File> dtoToEntity(List<FileDto> filesDto) {
        return filesDto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
