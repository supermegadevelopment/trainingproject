package edu.pro.converters;

import edu.pro.dtos.AdminDto;
import edu.pro.models.Admin;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AdminConverter implements Converter<Admin, AdminDto> {

    @Override
    public AdminDto entityToDto(Admin admin) {
        return AdminDto.builder()
                .id(admin.getId())
                .username(admin.getUsername())
                .password(admin.getPassword())
                .role(admin.getRole())
                .build();
    }

    @Override
    public List<AdminDto> entityToDto(List<Admin> admins) {
        return admins.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Admin dtoToEntity(AdminDto adminDto) {
        return Admin.builder()
                .id(adminDto.getId())
                .username(adminDto.getUsername())
                .password(adminDto.getPassword())
                .role(adminDto.getRole())
                .build();
    }

    @Override
    public List<Admin> dtoToEntity(List<AdminDto> adminsDto) {
        return adminsDto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
