package edu.pro.converters;

import edu.pro.dtos.LectureDto;
import edu.pro.models.Lecture;
import edu.pro.models.Test;
import edu.pro.repositories.FileRepo;
import edu.pro.repositories.TestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class LectureConverter implements Converter<Lecture, LectureDto> {

    private final FileRepo fileRepo;
    private final TestRepo testRepo;

    @Autowired
    public LectureConverter(FileRepo fileRepo, TestRepo testRepo) {
        this.fileRepo = fileRepo;
        this.testRepo = testRepo;
    }

    @Override
    public LectureDto entityToDto(Lecture lecture) {
        return LectureDto.builder()
                .id(lecture.getId())
                .topic(lecture.getTopic())
                .date(lecture.getDate())
                .fileId(lecture.getFile().getId())
                .isFinished(lecture.getIsFinished())
                .testIds(lecture.getTests().stream().map(Test::getId).collect(Collectors.toList()))
                .build();
    }

    @Override
    public List<LectureDto> entityToDto(List<Lecture> lectures) {
        return lectures.stream().map(this::entityToDto).collect(Collectors.toList());
    }

    @Override
    public Lecture dtoToEntity(LectureDto lectureDto) {
        return Lecture.builder()
                .id(lectureDto.getId())
                .topic(lectureDto.getTopic())
                .date(lectureDto.getDate())
                .file(fileRepo.findById(lectureDto.getFileId()).orElse(null))
                .isFinished(lectureDto.getIsFinished())
                .tests(lectureDto.getTestIds().stream().map(x -> testRepo.findById(x).orElse(null)).filter(Objects::nonNull).collect(Collectors.toList()))
                .build();
    }

    @Override
    public List<Lecture> dtoToEntity(List<LectureDto> lecturesDto) {
        return lecturesDto.stream().map(this::dtoToEntity).collect(Collectors.toList());
    }
}
