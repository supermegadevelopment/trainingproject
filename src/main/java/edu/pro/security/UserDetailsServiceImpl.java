package edu.pro.security;

import edu.pro.dtos.SecurityUser;
import edu.pro.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("UDSI")
public class UserDetailsServiceImplementation implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public UserDetailsServiceImplementation(UserService userService) {
        this.userService = userService;
    }

    @Override
    public SecurityUser loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userService.loadUserByUsername(username);
        if (user != null)
            return user;

        throw new UsernameNotFoundException(String.format("User with username %s doesn't exist", username));
    }
}
