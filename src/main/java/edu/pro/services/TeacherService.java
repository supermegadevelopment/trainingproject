package edu.pro.services;

import edu.pro.converters.TeacherConverter;
import edu.pro.dtos.TeacherDto;
import edu.pro.repositories.TeacherRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    private final TeacherRepo teacherRepo;
    private final TeacherConverter teacherConverter;

    @Autowired
    public TeacherService(TeacherRepo teacherRepo, TeacherConverter teacherConverter) {
        this.teacherRepo = teacherRepo;
        this.teacherConverter = teacherConverter;
    }

    public List<TeacherDto> findAll() {
        var teachers = teacherRepo.findAll();
        return teacherConverter.entityToDto(teachers);
    }

    public TeacherDto findById(Long id) {
        var teacher = teacherRepo.findById(id).orElse(null);
        if (teacher == null)
            return null;
        return teacherConverter.entityToDto(teacher);
    }

    public TeacherDto findByUsername(String username) {
        var teacher = teacherRepo.findByUsername(username).orElse(null);
        if (teacher == null)
            return null;
        return teacherConverter.entityToDto(teacher);
    }
}
