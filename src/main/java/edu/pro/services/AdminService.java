package edu.pro.services;

import edu.pro.converters.AdminConverter;
import edu.pro.dtos.AdminDto;
import edu.pro.repositories.AdminRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

    private final AdminRepo adminRepo;
    private final AdminConverter adminConverter;

    @Autowired
    public AdminService(AdminRepo adminRepo, AdminConverter adminConverter) {
        this.adminRepo = adminRepo;
        this.adminConverter = adminConverter;
    }

    public List<AdminDto> findAll() {
        var admins = adminRepo.findAll();
        return adminConverter.entityToDto(admins);
    }

    public AdminDto findById(Long id) {
        var admin = adminRepo.findById(id).orElse(null);
        if (admin == null)
            return null;
        return adminConverter.entityToDto(admin);
    }

    public AdminDto findByUsername(String username) {
        var admin = adminRepo.findByUsername(username).orElse(null);
        if (admin == null)
            return null;
        return adminConverter.entityToDto(admin);
    }
}
