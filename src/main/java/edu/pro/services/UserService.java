package edu.pro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final AdminService adminService;
    private final TeacherService teacherService;
    private final StudentService studentService;

    @Autowired
    public UserService(TeacherService teacherService, StudentService studentService, AdminService adminService) {
        this.adminService = adminService;
        this.teacherService = teacherService;
        this.studentService = studentService;
    }

    public UserDetails loadUserByUsername(String username) {
        var admin = adminService.findByUsername(username);
        if (admin != null)
            return admin;

        var student = studentService.findByUsername(username);
        if (student != null)
            return student;

        return teacherService.findByUsername(username);
    }
}
