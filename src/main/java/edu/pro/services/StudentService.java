package edu.pro.services;

import edu.pro.converters.StudentConverter;
import edu.pro.dtos.StudentDto;
import edu.pro.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepo studentRepo;
    private final StudentConverter studentConverter;

    @Autowired
    public StudentService(StudentRepo studentRepo, StudentConverter studentConverter) {
        this.studentRepo = studentRepo;
        this.studentConverter = studentConverter;
    }

    public List<StudentDto> findAll() {
        var students = studentRepo.findAll();
        return studentConverter.entityToDto(students);
    }

    public StudentDto findById(Long id) {
        var student = studentRepo.findById(id).orElse(null);
        if (student == null)
            return null;
        return studentConverter.entityToDto(student);
    }

    public StudentDto findByUsername(String username) {
        var student = studentRepo.findByUsername(username).orElse(null);
        if (student == null)
            return null;
        return studentConverter.entityToDto(student);
    }
}
