package edu.pro.models;

import edu.pro.security.Role;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "teachers",
        uniqueConstraints = @UniqueConstraint(columnNames = {"first_name", "last_name", "username"}, name = "unq_teacher_first_last_user_name"))
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Teacher extends BaseEntity<Long> {
    @Id
    @SequenceGenerator(name = "teachersSeq", sequenceName = "teachers_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teachersSeq")
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "is_not_banned")
    private Boolean isNotBanned;

    @OneToMany(mappedBy = "author")
    @Transient
    private List<Lecture> lectures;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
