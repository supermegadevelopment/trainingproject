package edu.pro.models;

import java.util.Objects;

public abstract class BaseEntity<T> {
    public abstract T getId();

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (!(o instanceof BaseEntity<?>))
            return false;
        return Objects.equals(this.getId(), ((BaseEntity<?>) o).getId());
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }
}
