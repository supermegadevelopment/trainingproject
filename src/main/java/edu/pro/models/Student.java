package edu.pro.models;

import edu.pro.security.Role;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "students",
        uniqueConstraints = @UniqueConstraint(columnNames = {"first_name", "last_name", "username"}, name = "unq_stud_first_last_user_name"))
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Student extends BaseEntity<Long> {
    @Id
    @SequenceGenerator(name = "studentsSeq", sequenceName = "students_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "studentsSeq")
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "student")
    @Transient
    private List<Answer> answers;

    @Column(name = "is_not_banned")
    private Boolean isNotBanned;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
