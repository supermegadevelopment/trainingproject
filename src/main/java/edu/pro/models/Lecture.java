package edu.pro.models;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;


@Entity
@Table(name = "lectures")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Lecture extends BaseEntity<Long> {
    @Id
    @SequenceGenerator(name = "lecturesSeq", sequenceName = "lectures_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lecturesSeq")
    @Column(name = "id")
    private Long id;

    @Column(name = "topic")
    private String topic;

    @Column(name = "lec_date")
    private ZonedDateTime date;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "file")
    private File file;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "author_id")
    private Teacher author;

    @Column(name = "is_finished")
    private Boolean isFinished;

    @OneToMany(mappedBy = "lecture", cascade = CascadeType.REMOVE)
    private List<Test> tests;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
