package edu.pro.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tests", uniqueConstraints = @UniqueConstraint(columnNames = {"lecture_id", "question"}))
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Test extends BaseEntity<Long> {
    @Id
    @SequenceGenerator(name = "testsSeq", sequenceName = "tests_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "testsSeq")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lecture_id")
    private Lecture lecture;

    @Column(name = "question")
    private String question;

    @OneToMany(mappedBy = "test")
    @Transient
    private List<Answer> answers;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
