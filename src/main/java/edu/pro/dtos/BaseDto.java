package edu.pro.dtos;

import edu.pro.models.BaseEntity;

import java.util.Objects;

public abstract class BaseDto<T> {

    public abstract T getId();

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (!(o instanceof BaseEntity<?>))
            return false;
        return Objects.equals(this.getId(), ((BaseEntity<?>) o).getId());
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }
}
