package edu.pro.dtos;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class TestDto extends BaseDto<Long>{

    private Long id;
    private Long lectureId;
    private String question;
    private List<Long> answerIds;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
