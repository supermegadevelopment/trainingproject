package edu.pro.dtos;

import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class LectureDto extends BaseDto<Long> {

    private Long id;
    private String topic;
    private ZonedDateTime date;
    private UUID fileId;
    private Boolean isFinished;
    private List<Long> testIds;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
