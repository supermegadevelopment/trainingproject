package edu.pro.dtos;

import lombok.*;

import java.time.ZonedDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class AnswerDto extends BaseDto<Long> {

    private Long id;
    private Long testId;
    private Long studentId;
    private ZonedDateTime date;
    private String answerText;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
