package edu.pro.controllers;

import edu.pro.dtos.AdminDto;
import edu.pro.security.Role;
import edu.pro.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class MainController {

    private final AdminService adminService;

    @Autowired
    public MainController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping(value = "/success")
    public void redirect(@AuthenticationPrincipal UserDetails user, HttpServletResponse response) throws IOException {
        if (user.getAuthorities().contains(Role.ADMIN))
            response.sendRedirect("/admin");
        else if (user.getAuthorities().contains(Role.STUDENT))
            response.sendRedirect("/student");
        else if (user.getAuthorities().contains(Role.TEACHER))
            response.sendRedirect("/teacher");
    }

    @GetMapping(value = "/admin", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AdminDto>> getAdmin() {
        var admins = adminService.findAll();
        return admins != null
                ? new ResponseEntity<>(admins, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
